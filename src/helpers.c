#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "csapp.h"
#include "threadpool.h"
#include "list.h"
#include "helpers.h"
#include <string.h>

//Typedefs
typedef struct list list;
typedef struct list_elem list_elem;
typedef struct memBlock{
list_elem elem;
char* mem;
}memBlock;

//Static variables
list memList;
static pthread_mutex_t listLock;
static char* cacheLoad;
static clock_t laClock; 
static double elapsed;

//Static methods
void responseWriter(int socket, char*version, char* res)
{
        header(socket, version, "Gibberish", strlen(res));
        rio_writen(socket, res, strlen(res));
}

//Initializes the required static variables
void initializer()
{
        list_init(&memList);
	pthread_mutex_init(&listLock, NULL);
	laClock = clock();
	elapsed = 0.0;
}

//HTTP header generator
void header(int sockFD, char* version, char* file, int filesize)
{
	char header[10000] = "";
	
	//Status code and content length
	strcat(header, version);
	strcat(header," 200 OK\r\nServer: Jishnu Renugopal and Mattin Zargarpur\r\nContent-Length:");
	sprintf(header, "%s %d\r\nContent-Type: ",header, filesize);
	
	//Set file type
	if(strstr(file,".html"))
		strcat(header, "text/html\r\n\r\n");
	else if(strstr(file,".doc"))
		strcat(header, "application/msword\r\n\r\n");
	else if(strstr(file,".css"))
		strcat(header, "text/css\r\n\r\n");
	else if(strstr(file,".js"))	
		strcat(header, "text/JavaScript\r\n\r\n");
	else
		strcat(header, "text/plain\r\n\r\n");	

	rio_writen(sockFD, header, strlen(header));
}
//Main method for the API.
int handleBuiltIn(char* command, char* arg, int socket, char* version)
{
	if(strncmp(command, "/runloop",8)==0)
	{
		//Runs a loop for 15 seconds
		runLoop(socket, version);
		return 1;		
	}
	else
	{
		//Calls the right method depending on the string passed
		if(strncmp(command, "/meminfo",8)==0 && (strncmp(command, "/meminfo",10)==0
			|| strncmp(command, "/meminfo?",9)==0))
		{
			#ifdef LOG
			responseWriter(0, version, meminfo(arg));
			#endif
			responseWriter(socket, version, meminfo(arg));
			return 2;
		}
		else if(strncmp(command, "/loadavg",8)==0&& (strncmp(command, "/loadavg",10)==0
                        || strncmp(command, "/loadavg?",9)==0))
                {
			#ifdef LOG
                        responseWriter(0, version, loadavgFast(arg));
			#endif
			responseWriter(socket, version, loadavgFast(arg));
			return 3;
		}
		else if(strncmp(command, "/allocanon",10)==0)
                {
			responseWriter(socket, version, allocanon());
			return 4;
		}
		else if(strncmp(command, "/freeanon",9)==0)
		{
			responseWriter(socket, version, freeanon());
			return 5;
		}
		else if(strncmp(command, "/files",6)==0)
		{
			command+=6;
			serveFile(command, socket, version,arg);
			return 6;	
		}
		else
		{	
			#ifdef LOG
			errorHeader(0,version,"Unsupported", strlen("Unsupported"));
			#endif
			errorHeader(socket,version,"Unsupported", strlen("Unsupported"));
			return 0;
		}
		//Send request
	}
}
//Sends the error header
void errorHeader(int sockFD, char* version, char* file, int filesize)
{
	//Sends a 404 when a junk request is sent
	char header[10000] = "";
	strcat(header, version);
        strcat(header," 404 NOT FOUND\r\nServer: Jishnu Renugopal and Mattin Zargarpur\r\nContent-Length:");
        sprintf(header, "%s %d\r\nContent-Type: ",header, filesize);
	strcat(header, "text/plain\r\n\r\n");
        rio_writen(sockFD, header, strlen(header));
	rio_writen(sockFD, "Unsupported", strlen("Unsupported"));	
}
//Handles the request when it is freeanon
char* freeanon()
{
	//Frees a block that was previously allocated
        int sz =  256* 1000000;
        memBlock* memToFree;
	pthread_mutex_lock(&listLock);
        if(list_size(&memList)>0)
	{
		memToFree = list_entry(list_pop_front(&memList), memBlock, elem);
		munmap(memToFree->mem, sz);
	}
	else
	{
		printf("No block to free!\n");
	}
	pthread_mutex_unlock(&listLock);
        return "Freed block";
}

//Handles the request when it is allocanon
char* allocanon()
{
	//Allocates a block of 256MB and pushes it to the list
        int sz =  256* 1000000;
	//Use mutex to prevent concurrency issues
        pthread_mutex_lock(&listLock);
	memBlock *tempBlock = malloc(sizeof(memBlock));
        tempBlock->mem = mmap(0, sz, PROT_READ|PROT_WRITE,MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
        memset(tempBlock->mem, 0, sz);
        list_push_front(&memList,&tempBlock->elem);
        pthread_mutex_unlock(&listLock);
	return "Block allocated";
}

//Handles the request when it is runloop
char* runLoop(int socket, char* version)
{
	time_t end = time(NULL)+15;
	char* res = "Running loop for 15 seconds";
	responseWriter(socket, version, res);
        while(time(NULL)<end);
	return res;
}
/*
 * A caching system to speed up loadavg
 */
char* loadavgFast(char* callBackArg)
{
	char* res;
	if(cacheLoad && (double)(clock() - laClock)/CLOCKS_PER_SEC<100)
	{
		//Uses cached value
		res = cacheLoad;	
	}
	else
	{
		//Reset clock and set static
		res =  loadavg();
		laClock = clock();
		cacheLoad = res;
	}
	if(callBackArg!=NULL && strlen(callBackArg)>0)
        {
		//Sets the callback argument if it is required
		char* tempOutput = malloc(10000);
                strcat(tempOutput, callBackArg);
                strcat(tempOutput, "(");
                strcat(tempOutput, res);
                strcat(tempOutput, ")");
                res = tempOutput;
        }
	return res;
}
//Handles the request when it is loadavg
char* loadavg()
{
	FILE* loadAvg = fopen("/proc/loadavg","r");
	char* json = malloc(10000);
	strcat(json,"{");
	char line[256]= "";
	fgets(line, sizeof(line), loadAvg);
	char a1[20]="";
	char a2[20]="";
	char a3[20]="";
	char* threads = malloc(20);
	char* curThreads;
	char* totThreads;
	char unWanted[20]="";
	//Parse the string to get loadavg
	sscanf(line, "%s %s %s %s %s",a1,a2, a3, threads, unWanted);
	int i=0;
	char* token;
	//Get thread information
	while ((token = strsep(&threads, "/")))
	{
		if(i==0)
			curThreads = token;
		else
			totThreads = token;
		i++;
	}
	//Create the JSON string	
	strcat(json, "\"total_threads\": \"");
	strcat(json, totThreads);
	strcat(json, "\", ");

	strcat(json, "\"loadavg\": [\"");
	strcat(json, a1);
	strcat(json, "\", \"");
	strcat(json, a2);
	strcat(json, "\", \"");
	strcat(json, a3);
	strcat(json, "\"], ");
	
	strcat(json, "\"running_threads\": \"");
        strcat(json, curThreads);
        strcat(json, "\"}");	
	
	free(threads);
	fclose(loadAvg);
	return json;
}
//Serves the requested file
void serveFile(char* filePath, int socket, char* version, char*root)
{
	#ifdef LOG
	printf("File Path:%s\n", filePath);
	printf("Root: %s\n",root);
	#endif
	//Check if the path is valid and allowed
	if(!strstr(filePath,".."))
	{
		#ifdef LOG
		printf("Entering if ..\n");
		#endif
		char finalPath[200] = "";
		strcat(finalPath, root);
		strcat(finalPath, filePath);
		struct stat file;
		#ifdef LOG
		printf("FINAL ACCESS PATH: %s\n",finalPath);
		#endif
		if(stat(finalPath, &file)<0)
		{
			clienterror(version,socket,"Unavailable", "404",  "Unavailable", "File not found");
			return;
		}
		int inFD = open(finalPath, O_RDONLY);
		header(socket, version, filePath, file.st_size);
		//Send the file
		if(sendfile(socket,inFD,NULL, file.st_size)<0)
		{
			perror("Send error\n");
			fprintf(stderr,"Send file failed");
		}
		close(inFD);
	}
	else
	{
		//Print error message if the access is unauthorized
		clienterror(version,socket,"Forbidden", "403",  "Forbidden", "Unauthorized access");
	}

}


//Handles the request when it is meminfo
char* meminfo(char* callBackArg)
{
	//Create JSON and open file
	char* json = malloc(10000);
	FILE* meminfo = fopen("/proc/meminfo","r");
        char line[256]="";
	strcat(json, "{");
    	while (fgets(line, sizeof(line), meminfo)) 
	{
    		char key[20]="";
                char value[20]="";
		char unwanted[20]="";
		//Get one item at a time from meminfo
		sscanf(line, "%s %s %s",key,value,unwanted);
		key[strlen(key)-1] = '\0';
		
		strcat(json, "\"");
		strcat(json, key);
		strcat(json, "\": \"");
		strcat(json, value);
		strcat(json, "\", ");	
		
	}
	//Trim the comma
	json[strlen(json)-2] = '\0';
	strcat(json, "}");
	//Add method call if call back argument is given
	if(callBackArg!=NULL &&strlen(callBackArg)>0)
        {
                char* tempOutput = malloc(10000);
                strcat(tempOutput, callBackArg);
                strcat(tempOutput, "(");
                strcat(tempOutput, json);
                strcat(tempOutput, ")");
        	json = tempOutput;
	}
	fclose(meminfo);
	return json;
}

/*
 *  clienterror - returns an error message to the client
 *  Obtained from tiny.c
 */
void clienterror(char* version, int fd, char *cause, char *errnum,
                 char *shortmsg, char *longmsg)
{
    char buf[MAXLINE], body[MAXBUF];
    sprintf(body, "<html><title>Tiny Error</title>");
    sprintf(body, "%s<body bgcolor=""ffffff"">\r\n", body);
    sprintf(body, "%s%s: %s\r\n", body, errnum, shortmsg);
    sprintf(body, "%s<p>%s: %s\r\n", body, longmsg, cause);
    sprintf(body, "%s<hr><em>Sysstatd Web server</em>\r\n", body);
    sprintf(buf, "%s %s %s\r\n",version, errnum, shortmsg);
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "Content-type: text/html\r\n");
    Rio_writen(fd, buf, strlen(buf));
    sprintf(buf, "Content-length: %d\r\n\r\n", (int)strlen(body));
    Rio_writen(fd, buf, strlen(buf));
    Rio_writen(fd, body, strlen(body));
}

/*
 * This method check if the method is supported.
 * Additionally, it also checks if the request is supported.
 * If not, an error message is sent back as a response
 */
int checkErrors(int connFd, char* method, char* req, char* version)
{
	if(strcasecmp(method, "GET") != 0)
	{
		clienterror(version, connFd, method, "501", "Not Implemented", 
			"The server has not implemented this method.");
		return -1;
	}
	if(strstr(req, "runloop") != 0
		&& strstr(req, "meminfo") != 0
		&& strstr(req, "loadavg") != 0
		&& strstr(req, "allocanon") != 0
		&& strstr(req, "freeanon") != 0
		&& strstr(req, "files") != 0)
	{
		clienterror(version,connFd, req, "404", "Not found",
			"Resource not found.");
		return -1;
	}

	return 0;
}
