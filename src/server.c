#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include "csapp.h"
#include "list.h"
#include "helpers.h"

#define PERS_CONN_TIMEOUT_SEC 10
#define BACKLOG_SIZE 5
#define DEFAULT_PORT 15132

//Typedefs
typedef struct {
	int connFd;
	char* root;
} RequestArgs;

//Extern
extern struct list memList;

//Function prototypes
void* handleRequest(void* args);
int parseRequest(char* req, char* methodBuf, char* urlBuf, char* versionBuf, char* cbBuf);

int main(int argc, char *argv[])
{
	initializer();
	char* portNum = "15132";
	char* rootDir = "./";
	socklen_t cliAddrLen;
	struct sockaddr_in clientAddr;


	//Check for a given port number. If given, use it. Else, use default.
	int option;
	while((option = getopt(argc, argv, "p:R:s")) > 0)
	{
		switch(option)
		{
			case 'p':
				portNum = optarg;
				break;
			case 's':
				break;
			case 'R':
				rootDir = optarg;
				printf("%s\n",optarg);	
				break;
		}
	}

	//Prepare server address.
	struct addrinfo* result;
	struct addrinfo hints;
	bzero((char*)&hints, sizeof(struct addrinfo));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;
	hints.ai_protocol = 0;
	getaddrinfo(NULL, portNum, &hints, &result);
	#ifdef LOG
	printf("Server address prepared.\n");
	#endif
	//Prepare socket.
	struct addrinfo* rp;
	int socketFd;
	for(rp = result; rp != NULL; rp = rp->ai_next)
	{
		socketFd = Socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if(socketFd == -1)
		{
			continue;
		}
		if(Bind(socketFd, rp->ai_addr, rp->ai_addrlen) == 0)
		{
			break; //Succesful bind.
		}
		close(socketFd);
	}
	#ifdef LOG
        printf("Socket prepared.\n");
        #endif
	Listen(socketFd, BACKLOG_SIZE);

	pthread_t thread_id;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	int connFd;
	
	//Main server loop.
	while(true)
	{
		cliAddrLen = sizeof(clientAddr);
		connFd = Accept(socketFd, (struct sockaddr *) &clientAddr, &cliAddrLen);
		RequestArgs* args = malloc(sizeof(RequestArgs));
		args->connFd = connFd;
		args->root = rootDir;

		pthread_create(&thread_id, &attr, handleRequest, args);
	}

	exit(0);
}
//Handles the request
void* handleRequest(void* args)
{
	//Detaches the thread for auto cleanup by OS
	pthread_detach(pthread_self());
	RequestArgs* reqArgs = (RequestArgs*) args;
	int connFd = reqArgs->connFd;
	char* root = reqArgs->root;

	char rioBuf[MAXLINE];
	char method[MAXLINE];
	char url[MAXLINE];
	char version[MAXLINE];
	char callback[MAXLINE];

	rio_t rio;
	Rio_readinitb(&rio, connFd);
	int linesRead = 0;
	int charsRead = 0;

	do
	{
		int result = 0;
		while((charsRead = rio_readlineb(&rio, rioBuf, MAXLINE)) > 0)
		{
			if(linesRead == 0)
			{
				//Read header
				result = parseRequest(rioBuf, method, url, version, callback);
			}
			else if(charsRead <= 2)
			{
				break; // read empty line
			}
			linesRead++;
		}
		linesRead = 0;

		if(charsRead <= 0)
		{
			break;
		}

		if(charsRead > 0)
		{
			//Calls the appropriate method depending on the request
			if(checkErrors(connFd, method, url,version) == 0)
			{
				if(result == 1)
				{
					handleBuiltIn(url, root, connFd, version);
				}
				else
				{
					handleBuiltIn(url, callback, connFd, version);
				}
			}
		}
		
	} while(strcasecmp(version, "HTTP/1.1") == 0);//For persistent connections

	close(connFd);
	free(reqArgs);
	return NULL;
}
//Parses the request to obtain the method, the request and the callback argument
int parseRequest(char* req, char* methodBuf, char* urlBuf, char* versionBuf, char* cbBuf)
{
	//Checks for long requests
	if(strlen(req)>2000)
	{
		#ifdef LOG
		printf("Junk Request going to send 404\n");
		#endif
		methodBuf  = "GET";
                urlBuf = "/junk";
                versionBuf = "HTTP/1.0";
                return 0;
	}

	char *tokBuf = malloc(2000);
	sscanf(req, "%s %s %s", methodBuf, urlBuf, versionBuf);
	if(strstr(urlBuf,"files/"))
	{
		return 1;
	}
	//Sets the URL
	char tempURL[2000] = "";
	strcpy(tempURL, urlBuf);
	char* savePt;
	tokBuf = strtok_r(tempURL, "?&=_",&savePt);
	
	while((tokBuf = strtok_r(NULL, "?&=_",&savePt)))
	{
		if(strcmp(tokBuf, "callback")==0)
		{
			//Checks for callback argument
			strcpy(cbBuf,strtok_r(NULL, "?&=_",&savePt));
			return 2;
		}
	}
	return 0;
}
