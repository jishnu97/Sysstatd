Group Members:
	Jishnu Renugopal (jishnu)
	Mattin Zargarpur (mattinz)

Design:
	- After successfully dual-binding to a socket, the server loops indefinitely to do the following:
		- Block on a call to accept() until a connection is made.
		- Gather information necessary to handle the request.
		- Create a new thread to handle the request.
	- Threads which are created to handle incoming requests do the following:
		- Parse the request header to determine the nature of the request.
		- Call the appropriate helper function to perform the requested action and generate the subsequent response.
		- If the request is incorrect, respond with an error code.
		- If the request was made using HTTP/1.1, maintain the connection until all requests are completed or the connection is closed.

	Note that we elected not to implement our threadpool from project 2. Instead, we spawn a detached thread for every incoming connection. Once the thread has compelted its task, it is automatically reclaimed by the OS. This allows for a simpler program structure without, based on our testing, compromising server performance. 
