#include "threadpool.h"
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include "list.h"
#include <stdlib.h>
//Typedefs
typedef struct list list;
typedef struct worker worker;
typedef struct thread_pool thread_pool;
typedef struct list_elem list_elem;
typedef struct future future;
//Structs
struct worker 
{
	list localQueue;
	pthread_t ID;
	list_elem elem;
	bool isInternal;
};
struct future 
{
        void* ret;
        void* args;
        thread_pool* tPool;
        pthread_cond_t isDone; //Condition variable to signal that the job has been completed
        list_elem elem;
	pthread_mutex_t futLock;
        int stat;//The status of the job
	fork_join_task_t job;
};
struct thread_pool 
{
	list globalQueue;
	list workers;
	pthread_barrier_t isCreated;//Used to wait for all threads to be created
				    //Prevents race conditions
	pthread_cond_t cond;
	pthread_mutex_t threadLock;
	bool isShuttingDown;
	int numT;
};

static __thread worker* curThread;//This thread local variable is stored for easy access to the current thread

//Returns a task either by removing one from the queues or by stealing from an other thread
static list_elem* getTask(thread_pool* pool)
{
	if(!list_empty(&curThread->localQueue))
        {
		//Returns a task from the current thread's local queue
                return list_pop_back(&curThread->localQueue);
        }
        if (!list_empty(&pool->globalQueue))
        {
		//Returns a task from the global queue
                return list_pop_back(&pool->globalQueue);
        }
	//Steals a task from one of the other threads
        worker*  tempW;
        list_elem* e;
        for (e = list_begin(&pool->workers); e != list_end(&pool->workers); e = list_next(e))
        {
                tempW = list_entry(e, worker, elem);
                if (!list_empty(&tempW->localQueue))
                {
                        return list_pop_front(&tempW->localQueue);
                }
        }
	return NULL;
}

//Returns true of the pool has no tasks for the thread
static bool hasTasks(thread_pool* pool)
{
	//Checks if the global queue has tasks
	if(!list_empty(&pool->globalQueue))
		return false;
	//Checks if the local queue has tasks
	if(!list_empty(&curThread->localQueue))
		return false;
	//Checks if the pool is sutting down
	if(pool->isShuttingDown)
		return false;
	list_elem* e;
	worker*  tempW;
	//Checks if tasks can be stolen
	for (e = list_begin(&pool->workers); e != list_end(&pool->workers); e = list_next(e)) 
	{
		tempW = list_entry(e, worker, elem);
		if (!list_empty(&tempW->localQueue)) 
			return false;
	}
	return true;
}

//Finds the thread and assigns it to the thread local variable
static void findThread(thread_pool* pool)
{
	pthread_barrier_wait(&pool->isCreated);
	pthread_t ID = pthread_self();
	list_elem* e;
	worker* tempW;
	//Iterates through the list of threads to find the current thread
	for (e = list_begin(&pool->workers); e != list_end(&pool->workers); e = list_next(e)) 
	{
		tempW = list_entry(e, worker, elem);
                if (ID == tempW->ID) 
		{
			curThread = tempW;
			curThread->isInternal = true;
			return;
                }
	}
}
//The function that each thread runs
static void* threadFunc(void* args) 
{
	thread_pool* pool = (thread_pool*) args;
	findThread(pool);
	bool shutdown = false;
	while (!shutdown) 
	{
		pthread_mutex_lock(&pool->threadLock);
        	shutdown = pool->isShuttingDown;
		while(hasTasks(pool)) 
		{
			// Waits 
        		pthread_cond_wait(&pool->cond, &pool->threadLock);
        	}
		//Gets a task
        	list_elem* e;
		//Shuts down once the pool has no more tasks
        	if ((e=getTask(pool)) != NULL) 
        	{
			//Runs the future
        		future* fut = list_entry(e, future, elem);
			fut->stat = 1;
        		pthread_mutex_unlock(&pool->threadLock);
        		fut->ret = (fut->job)(pool, fut->args);
        		pthread_mutex_lock(&pool->threadLock);
        		fut->stat = 2;
        		//Signals that the task has finished execution
			pthread_cond_signal(&fut->isDone);
        		pthread_mutex_unlock(&pool->threadLock);   
		}
		else
		{
			pthread_mutex_unlock(&pool->threadLock);return NULL;
		}
	}
	return NULL;
}


//Creates a threadpool with numthreads
struct thread_pool* thread_pool_new(int numThreads) 
{
   	//Allocations and initializations
	thread_pool* pool;
    	pool = malloc(sizeof(thread_pool));
    	pthread_mutex_init(&pool->threadLock, NULL);
    	pthread_cond_init(&pool->cond, NULL);
	pthread_mutex_lock(&pool->threadLock);
	pool->numT = numThreads;
    	pthread_barrier_init(&pool->isCreated, NULL, pool->numT+1);
	list_init(&pool->workers);
    	list_init(&pool->globalQueue);
    	pool->isShuttingDown = false;   
	//Creates the threads
    	for (int i = 0;i < pool->numT; i++) 
	{
    	    	worker* tempThread;
        	tempThread = malloc(sizeof(worker));
        	list_push_back(&pool->workers, &tempThread->elem);
        	list_init(&tempThread->localQueue);
        	pthread_create(&tempThread->ID, NULL, threadFunc, pool);
    	}
	pthread_cond_broadcast(&pool->cond);
    	curThread = malloc(sizeof(worker));
    	curThread->ID = pthread_self();
    	list_init(&curThread->localQueue);
    	//Sets iis internal to false
	curThread->isInternal = false;
    	pthread_mutex_unlock(&pool->threadLock);
	//Waits for all threads to be created
	pthread_barrier_wait(&pool->isCreated);
	return pool;
}
//Shuts down the threadpool. Running tasks run to completion. 
void thread_pool_shutdown_and_destroy(thread_pool* pool) 
{
	pthread_mutex_lock(&pool->threadLock);
    	pool->isShuttingDown = true;
    	pthread_cond_broadcast(&pool->cond);
    	pthread_mutex_unlock(&pool->threadLock);
    	list_elem* e;
    	worker* tempThread;
	//Waits for all threads to join
	for (e = list_begin(&pool->workers); e != list_end(&pool->workers); e = list_next(e)) 
	{
    		tempThread = list_entry(e, worker, elem);
        	pthread_join(tempThread->ID, NULL);
    	}
	//Frees the workers
    	while (!list_empty(&pool->workers)) 
        	free(list_entry(list_pop_back(&pool->workers), worker, elem));
	free(curThread);
	//Free the threadpool
    	free(pool);
}
//Returns the result of the future
void* future_get(future* fut) 
{
	pthread_mutex_lock(&fut->tPool->threadLock);
	//Waits for the job to be completed if it is in progress
	if (fut->stat != 0) 
	{
		while (fut->stat <= 1)
                        pthread_cond_wait(&fut->isDone, &fut->tPool->threadLock);
		void* ret = fut->ret;
        	pthread_mutex_unlock(&fut->tPool->threadLock);
        	return ret;
	} 
	//RUns the task as its own if the task hasn't started yet
       	fut->stat = 1;
	list_remove(&fut->elem);
        pthread_mutex_unlock(&fut->tPool->threadLock);
        fut->ret = (fut->job)(fut->tPool, fut->args);
        pthread_mutex_lock(&fut->tPool->threadLock);
        fut->stat = 2; 
	void* ret = fut->ret;
    	pthread_mutex_unlock(&fut->tPool->threadLock);
    	return ret;
}
struct future* thread_pool_submit(thread_pool* tPool,  fork_join_task_t job, void* args) 
{
    	pthread_mutex_lock(&tPool->threadLock);
    	//Initializes and allocates the future
	future* fut;
   	fut = malloc(sizeof(future));
    	pthread_mutex_init(&fut->futLock, NULL);
	pthread_mutex_lock(&fut->futLock);	
	fut->stat = 0;
    	fut->tPool = tPool;
	pthread_cond_init(&fut->isDone, NULL);
	fut->job = job;
        fut->args = args;
    	//Pushes to the appropriate list depending on whether the submission is internal or not
	if (!curThread->isInternal) 
	{
		list_push_front(&tPool->globalQueue, &fut->elem);
    	} 
	else 
	{
		list_push_back(&curThread->localQueue, &fut->elem);
	}
	//Signals the thread pool to return the result
    	pthread_cond_signal(&tPool->cond);
    	pthread_mutex_unlock(&fut->futLock);
	pthread_mutex_unlock(&tPool->threadLock);
    	return fut;
}


//Frees the future
void future_free(future* fut) 
{
    free(fut);
}
